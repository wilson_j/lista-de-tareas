import { Component, OnInit } from '@angular/core';
import { Tarea } from './../models/tarea.model';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {
  tareas: Tarea[];

  constructor() {
    this.tareas = [];
  }

  ngOnInit(): void {
  }

  guardar( desc: string ): boolean {
    this.tareas.push( new Tarea( desc ) );
    console.log(this.tareas);
    
    return false;
  } 

}
