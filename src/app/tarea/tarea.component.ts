import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Tarea } from '../models/tarea.model';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css']
})
export class TareaComponent implements OnInit {
  @Input() tarea: Tarea;
  @HostBinding('attr.class') cssClass = 'col-md-12';
  constructor() { }

  ngOnInit(): void {
  }

}
